# WWU  Media Fix

The media module has a
[well documented flaw](https://www.drupal.org/node/2194821#comment-9773895)
with how it renders media in Drupal when the media was uploaded prior
to February 12th 2014.

A [gist was published](https://www.drupal.org/node/2194821#comment-9515943)
that reverts this behavior.

The long term solution is still being worked out by the community. Until that
time we are making the above gist a module that will be enabled by default in
our installations to fully support legacy media.

## To retrofit existing sites

* Clone this module into sites/all/modules
* Enable the module on your website either via the web interface or with drush.
